<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group([

    // 'middleware' => 'api',
    'namespace' => 'App\Http\Controllers\Api',
    'prefix' => 'v1'

], function ($router) {

    // Route::post('refresh', 'Auth\AuthController@refresh');
    
    Route::post('obtainToken', 'Auth\AuthenticationController@obtainToken');
    Route::post('logout', 'Auth\AuthenticationController@logout');

    Route::get('userinfo', 'UserController@userInfo');
    
    // Route::post('application/getData', 'ApplicationDatasController@getData');
    // Route::post('application/writeData', 'ApplicationDatasController@storeRow');
    
});