<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ApplicationData extends Model
{
    use HasFactory;

    protected $table = 'applications_data';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'model',
        'data'
    ];

    public function setDataAttribute($value) {
        $this->attributes['data'] = json_encode($value);
    }

    public function getDataAttribute($value) {
        if($value)
            return json_decode($value, true);
        
        return [];
    }
}
