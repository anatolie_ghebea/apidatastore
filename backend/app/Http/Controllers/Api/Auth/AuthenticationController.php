<?php

namespace App\Http\Controllers\Api\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\Controller;
use App\Models\User;
// use CustomAPIToken\TokenService;
use App\Helpers\CustomApiTokenHelper;
use Illuminate\Support\Facades\Hash;

class AuthenticationController extends Controller
{
    //

    public function obtainToken(Request $request){

        $request->validate([
            'nickname' => 'required|string',
            'password' => 'required|string'
        ]);

        $user = User::query()->where('nickname', '=', $request->nickname )->first();
        if( !$user ){
            return response()->json([ RESP_STATUS => false, RESP_ERR => __('User not found') ], 400 );
        }   

        if( !Hash::check($request->password, $user->password) ){
            return response()->json([ RESP_STATUS => false, RESP_ERR => __('Invalid credentials') ], 400 );
        }

        $tokenService = CustomApiTokenHelper::tokenServiceInstance();
        $token = $tokenService->createUserToken($user->nickname, $user->id);

        $user->access_token = $token;
        if( !$user->save() )
            return response()->json([ RESP_STATUS => false, RESP_ERR => __('Auhtentication failed') ], 400 );
        
        return response()->json([ RESP_STATUS => true, RESP_DATA => [ 'token' => $token ] ]);
    }

    public function logout(Request $request ){

        $this->current_application_user->access_token = '';
        if( $this->current_application_user->save() ){
            return response()->json( [ RESP_STATUS => true, RESP_MSG => __('You have been successfully logged out.')] );
        }

        return response()->json([ RESP_STATUS => false, RESP_ERR => __('Some error occured during loguot, please retry later.')], 500 );
    }
}
