<?php

namespace App\Http\Controllers\Api;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
// use App\Helpers\CustomApiTokenHelper;
use App\Models\User;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $current_application_user = null;

    public function __construct()
    {
        
        $this->middleware( function($request, $next) {

            // 
            $token = $request->bearerToken();
            ray($token);

            if( !$token ) {
                return response()->json([ RESP_STATUS => false, RESP_ERR => __('Access token absent')], 400);
            }

            
            //
            // check if token exists
            // 
            $user = User::query()->where('access_token', '=', $token)->first();
            if( !$user ) {
                return response()->json([ RESP_STATUS => false, RESP_ERR => __('Invalida token, try to login again.')], 400);
            }

            // TODO check if token is expired.

            //
            // $tokenService = CustomApiTokenHelper::tokenServiceInstance();
            // 

            $this->current_application_user = $user;

            return $next($request);

        })->except('obtainToken');
    }
}
