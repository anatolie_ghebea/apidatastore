<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\Controller;
use Illuminate\Http\Client\ResponseSequence;
use Illuminate\Http\Request;

class UserController extends Controller
{
    
    public function userInfo(){

        // $user = $this->getUser();

        // reset password
        // just a quick work around
        // $user->password = bcrypt('JcPjSlModi');
        // $user->save();
    
        return response()->json(['user' => $this->current_application_user ]);

    }
}
