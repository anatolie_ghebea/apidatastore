<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\Controller;
use Illuminate\Http\Request;
use App\Models\ApplicationData;

class ApplicationDatasController extends Controller
{
    //

    public function getData(Request $request){

        $requestData = $this->checkRequestData( $request->all() );
        if(!$requestData){
            return response()->json(['error' => __('Incorect request')]);
        }

        $model = $requestData['model'];
        // $person = $requestData['person_name'];

        $rows = ApplicationData::query()->where('application', '=', $this->current_application_user->name)->where('model', '=', $model )->get();

        return response()->json(['data' => $rows]);

    }

    public function storeRow(Request $request){
        $application = $this->current_application_user;
        $requestData = $this->checkRequestData( $request->all() );

        if(!$requestData){
            return response()->json(['error' => __('Incorect request')]);
        }

        $row = new ApplicationData();

        $row->application = $application->name;
        $row->model = $requestData['model'];
        // $row->data = json_encode($requestData['data']);
        $row->data = $requestData['data'];//not working
        // $row->person = $requestData['person_name'];

        if( !$row->save() )
            return response()->json(['error' => __('Write data failed, please try again.')], 404 );
        
        return response()->json(['message' => 'Data persisted.', 'data' => $row ]);
    }

    public function editRow(Request $request){

    }

    public function deleteRow(Request $request){

    }

    protected function checkRequestData($requestData = [] ){
        if( !is_array($requestData) || count($requestData) == 0 ){
            return false;
        }

        $requiredParams = ['model', 'data'];
        // $optionalParams = [ 'person_name', 'model_id', 'query_filter'];

        foreach( $requiredParams as $param ){
            if( !isset( $requestData[$param] ) )
                return false;
        }

        // foreach( array_keys($requestData) as $k ){
        //     if( !in_array($k, $requiredParams) && !in_array($k, $optionalParams))
        //         unset($requestData[$k]);
        // }

        return $requestData;

    }

}
