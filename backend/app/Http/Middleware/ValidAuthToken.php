<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class ValidAuthToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        ray('this middleware must check the user token');
        return $next($request);
    }
}
