<?php 

namespace App\Helpers;

use CustomAPIToken\TokenService;

class CustomApiTokenHelper {

    private $tokenService = null;

    public function __construct()
    {
        $tokenLongevity = config('app.sso_token_longevity');
        $algo = config('app.sso_token_algo');
        $sharedKey = config('app.sso_token_shared_key');
        
        $encryption_key = config('app.aes_realm_key');
        $initialization_vector = config('app.aes_realm_iv');

        
        ray(config('app.env'));
        ray($sharedKey);

        $this->tokenService = new TokenService($sharedKey, $encryption_key, $initialization_vector, $tokenLongevity, $algo);
    }
    
    public function getTokenService(){
        return $this->tokenService;
    }

    public static function tokenServiceInstance(){
        return app( CustomApiTokenHelper::class )->getTokenService();
    }


}

