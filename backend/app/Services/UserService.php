<?php 

namespace App\Services;
use App\Models\User;
use Illuminate\Http\Request;

class UserService {


    /**
     * @param Illuminate\Http\Request $request      The request from the client
     * @return  App\Models\User                     If a valid user is found
     * @return  JsonResponse                        On error (if request expects Json response)
     * @return  RedirectResponse                    On error (if request doesn't expects Json response)
     */
    public static function getUserForRequest(Request $request){
        $jsonResponse = $request->expectsJson();

        if (!$email = $request->email) {
            $error = __('User email is required, but was not found in the request data.');
            
            return $jsonResponse 
                ? response()->json(['status' => false, 'error' => $error ], 400)
                : redirect()->back()->with('error', $error );
		}

		if (!$user = User::query()->where('email', '=', $email)->first()) {
            $error = __('User not found');

            return $jsonResponse 
                ? response()->json(['status' => false, 'error' => $error ], 400)
                : redirect()->back()->with('error', $error );
		}

        return $user;
    }

    /**
     * 
     */
    public static function getCustomApiTokenInstance(){
        


    }

}